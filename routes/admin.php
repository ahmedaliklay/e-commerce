<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admin','namespace'=>'Admin'],function(){
    
    Config::set('auth.defines', 'admin');
    
    Route::get('login','AdminAuth@login');

    Route::post('login','AdminAuth@doLogin');

    Route::get('reset/password/{token}','AdminAuth@reset_password');

    Route::post('reset/password/{token}','AdminAuth@reset_password_got');

    Route::get('forgot-password','AdminAuth@forgot_password');
    Route::post('forgot_password_post','AdminAuth@forgot_password_post');
    Route::group(['middleware'=>'admin:admin'],function(){

        Route::get('/', function () {
            return view('Admin.Home');
        });
        Route::get('logout','AdminAuth@logOut');

    });
    
    
});
