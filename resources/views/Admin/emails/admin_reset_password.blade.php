@component('mail::message')
# Reset Account
welcome {{ $data['data']->name }}
<br>
The body of your message.

@component('mail::button', ['url' => aurl('reset/password/'.$data['token'])])
Click Here To Reser Your Password
@endcomponent
<hr>
<div class="border-top border-bottom">
    <h1>or</h1>
    copy this link :
    <a href="{{ aurl('reset/password/'.$data['token']) }}">{{aurl('reset/password/'.$data['token'])}}</a>
</div>
<hr>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
{{-- 
## commind line :

php artisan make:mail AdminResetPassword --markdown=admin.emails.admin_reset_password

--}}